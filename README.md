[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# F-Droid Repo
Repository for installing apps more easily.

## Updates
This repository is updated every day at 00:00 UTC+2. If updates are available at this time, you will receive them after the building process is completed.

## Apps
The following apps made by others are available from this repo:
- [GrapheneOS Apps](https://github.com/GrapheneOS/AppStore), [GitHub releases](https://github.com/GrapheneOS/AppStore/releases)
- [GrapheneOS Auditor](https://github.com/GrapheneOS/Auditor), [GitHub releases](https://github.com/GrapheneOS/Auditor/releases)
- [GrapheneOS Camera](https://github.com/GrapheneOS/Camera), [GitHub releases](https://github.com/GrapheneOS/Camera/releases)
- [GrapheneOS Info](https://github.com/GrapheneOS/Info), [GitHub releases](https://github.com/GrapheneOS/Info/releases)
- [GrapheneOS PDF Viewer](https://github.com/GrapheneOS/PdfViewer), [GitHub releases](https://github.com/GrapheneOS/PdfViewer/releases), [request for packaging in F-Droid (closed)](https://gitlab.com/fdroid/rfp/-/issues/1163)
- [Proton Calendar](https://github.com/ProtonCalendar), [APK](https://protonapps.com/protoncalendar-android)
- [Proton Drive](https://github.com/ProtonDriveApps/android-drive), [APK](https://protonapps.com/protondrive-android), [request for packaging in F-Droid](https://gitlab.com/fdroid/rfp/-/issues/2380)
- [Proton Mail](https://github.com/ProtonMail/mail-android), [GitHub releases](https://github.com/ProtonMail/mail-android/releases), [request for packaging in F-Droid (closed)](https://gitlab.com/fdroid/rfp/-/issues/1323)
- [Signal](https://github.com/signalapp/Signal-Android), [APK](https://signal.org/android/apk), [request for packaging in F-Droid (closed)](https://gitlab.com/fdroid/rfp/-/issues/451)

### Anti-Features
- Proton Calendar
  - Non-free network services (Proton servers)
- Proton Drive
  - Non-free network services (Proton servers)
- Proton Mail
  - Non-free network services (Proton servers)
  - Non-free dependencies ([Google Play Services](https://github.com/search?q=repo%3AProtonMail%2Fproton-mail-android+%22com.google.android.gms.%22&type=code))
- Signal
  - Non-free dependencies ([Google Play Services](https://github.com/search?q=repo%3Asignalapp%2FSignal-Android+%22com.google.android.gms.%22&type=code))

## How to use
Copy this link and add it to your F-Droid client:

```
https://julianfairfax.gitlab.io/fdroid-repo/fdroid/repo?fingerprint=83ABB548CAA6F311CE3591DDCA466B65213FD0541352502702B1908F0C84206D
```

You can also scan this QR code and add the link to your F-Droid client:

![F-Droid repo QR code](https://julianfairfax.gitlab.io/fdroid-repo/fdroid/repo/index.png)

## Licensing information
The apps hosted on this repository are under the licenses of their developers. This repository's license only applies to the code for the repository itself.

If you believe your code has been unjustly used or has been used without proper credit in this repository, please [contact](https://julianfairfax.gitlab.io/contact.html) me.

## Self-hosting information
If you are going to host a repository like this yourself, it is requested that you remove my content from it and replace it with you own. This includes references to my name, projects, or website.

You must also set `KEYSTORE`, `KEYALIAS`, `PASS`, and `KEYDNAME` GitLab variables or GitHub secrets, containing the contents of your keystore file, encoded in Base64, its key alias, its password, and its keydname.

You should edit the `config.yml` file to change the repo's URL, name, and description. You can add additional information by looking at the [example](https://gitlab.com/fdroid/fdroidserver/-/blob/master/examples/config.yml).

You can create the keystore file by using keytool: `keytool -genkey -v -keystore keystore.keystore -alias key -keyalg RSA -keysize 4096 -validity 100000`. To list its contents, run `keytool -list -v -keystore keystore.keystore`.

You must also modify this README to replace references to my name, projects, and website with your own.

Under the terms of the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/) license used by this project, you are required to use this same license in any and all redistributions of this project's code.

If you wish to host a repository like this on GitHub Pages instead of GitLab Pages, you can do so without issue, as this one is made to be compatible.

